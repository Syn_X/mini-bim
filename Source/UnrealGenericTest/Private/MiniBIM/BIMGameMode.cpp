// Fill out your copyright notice in the Description page of Project Settings.


#include "MiniBIM/BIMGameMode.h"
#include "MiniBIM/BIMPlayerController.h"
#include "MiniBIM/BIMFloatingPawn.h"
#include "MiniBIM/UI/BaseHUD.h"

ABIMGameMode::ABIMGameMode()
{
	DefaultPawnClass = ABIMFloatingPawn::StaticClass();
	//PlayerControllerClass = ABIMPlayerController::StaticClass();
}


void ABIMGameMode::BeginPlay()
{
	Super::BeginPlay();

	APlayerController* playerController = GetWorld()->GetFirstPlayerController();

	playerController->bShowMouseCursor = true;
	playerController->bEnableClickEvents = true;
	playerController->bEnableMouseOverEvents = true;
}
