#include "MiniBIM/BIMProceduralMeshComponent.h"


// Sets default values for this component's properties
UBIMProceduralMeshComponent::UBIMProceduralMeshComponent()
{

	PrimaryComponentTick.bCanEverTick = false;

	_Mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("BIMProceduralMesh"));
	_Mesh->SetRenderCustomDepth(true);
	_Mesh->SetupAttachment(GetAttachmentRoot());
}


void UBIMProceduralMeshComponent::ResetMesh()
{
	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	Tangents.Reset();
	UVs.Reset();
	Colors.Reset();
}


/*
*	Change CustomDepthStencilValue to enable post-processing material highlighting
*/
void UBIMProceduralMeshComponent::Select()
{
	_Mesh->CustomDepthStencilValue = 2;
	_Mesh->MarkRenderStateDirty();
}


/*
*	Change CustomDepthStencilValue to disable post-processing material highlighting
*/
void UBIMProceduralMeshComponent::Deselect()
{
	_Mesh->CustomDepthStencilValue = 0;
	_Mesh->MarkRenderStateDirty();
}

/*
*	 Generated mesh with dimensions [+X/2,-X/2], +[Y/2,-Y/2], [0,Z]
*/
void UBIMProceduralMeshComponent::GenerateMesh()
{
	int32 TriangleIndexCount = 0;
	FVector DefinedShape[8];
	FProcMeshTangent TangentSetup;

	FVector halfDimVector = _Dimensions * 0.5;


	DefinedShape[0] = FVector(halfDimVector.X, halfDimVector.Y, _Dimensions.Z);
	DefinedShape[1] = FVector(halfDimVector.X, halfDimVector.Y, 0);
	DefinedShape[2] = FVector(halfDimVector.X, -halfDimVector.Y, _Dimensions.Z);
	DefinedShape[3] = FVector(halfDimVector.X, -halfDimVector.Y, 0);
	DefinedShape[4] = FVector(-halfDimVector.X, -halfDimVector.Y, _Dimensions.Z);
	DefinedShape[5] = FVector(-halfDimVector.X, -halfDimVector.Y, 0);
	DefinedShape[6] = FVector(-halfDimVector.X, halfDimVector.Y, _Dimensions.Z);
	DefinedShape[7] = FVector(-halfDimVector.X, halfDimVector.Y, 0);

	//front
	TangentSetup = FProcMeshTangent(0, 1, 0);
	AddQuadMesh(DefinedShape[0], DefinedShape[1], DefinedShape[2], DefinedShape[3], TriangleIndexCount, TangentSetup);
	//left
	TangentSetup = FProcMeshTangent(1, 0, 0);
	AddQuadMesh(DefinedShape[2], DefinedShape[3], DefinedShape[4], DefinedShape[5], TriangleIndexCount, TangentSetup);
	//back
	TangentSetup = FProcMeshTangent(0, -1, 0);
	AddQuadMesh(DefinedShape[4], DefinedShape[5], DefinedShape[6], DefinedShape[7], TriangleIndexCount, TangentSetup);
	//right
	TangentSetup = FProcMeshTangent(-1, 0, 0);
	AddQuadMesh(DefinedShape[6], DefinedShape[7], DefinedShape[0], DefinedShape[1], TriangleIndexCount, TangentSetup);
	//top
	TangentSetup = FProcMeshTangent(0, 1, 0);
	AddQuadMesh(DefinedShape[6], DefinedShape[0], DefinedShape[4], DefinedShape[2], TriangleIndexCount, TangentSetup);
	//bottom
	TangentSetup = FProcMeshTangent(0, -1, 0);
	AddQuadMesh(DefinedShape[1], DefinedShape[7], DefinedShape[3], DefinedShape[5], TriangleIndexCount, TangentSetup);

	_Mesh->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UVs, Colors, Tangents, true);
}


void UBIMProceduralMeshComponent::SetDimensions(FVector dimensions)
{
	_Dimensions = dimensions.GetAbs();
}


const FVector UBIMProceduralMeshComponent::GetDimensions()
{
	return _Dimensions;
}


/*
*	UNUSED
*/
void UBIMProceduralMeshComponent::AddTriangleMesh(FVector TopRight, FVector BottomRight, FVector BottomLeft, int32& TriIndex, FProcMeshTangent Tangent)
{
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;

	Vertices.Add(TopRight);
	Vertices.Add(BottomRight);
	Vertices.Add(BottomLeft);

	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);

	FVector ThisNorm = FVector::CrossProduct(TopRight, BottomRight).GetSafeNormal();
	for (int i = 0; i < 3; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(Tangent);
		Colors.Add(FLinearColor::Green);
	}

	UVs.Add(FVector2D(0, 1));
	UVs.Add(FVector2D(0, 0));
	UVs.Add(FVector2D(1, 0));

}


/*
*	Creates a quad mesh, with two triangles and 4 vertices
*/
void UBIMProceduralMeshComponent::AddQuadMesh(FVector TopRight, FVector BottomRight, FVector TopLeft, FVector BottomLeft, int32& TriIndex, FProcMeshTangent Tangent)
{
	int32 Point1 = TriIndex++;
	int32 Point2 = TriIndex++;
	int32 Point3 = TriIndex++;
	int32 Point4 = TriIndex++;

	Vertices.Add(TopRight);
	Vertices.Add(BottomRight);
	Vertices.Add(TopLeft);
	Vertices.Add(BottomLeft);

	Triangles.Add(Point1);
	Triangles.Add(Point2);
	Triangles.Add(Point3);

	Triangles.Add(Point4);
	Triangles.Add(Point3);
	Triangles.Add(Point2);

	FVector ThisNorm = FVector::CrossProduct(TopLeft - BottomRight, TopLeft - TopRight).GetSafeNormal();
	for (int i = 0; i < 4; i++)
	{
		Normals.Add(ThisNorm);
		Tangents.Add(Tangent);
		Colors.Add(FLinearColor::Green);
	}

	UVs.Add(FVector2D(0, 1));
	UVs.Add(FVector2D(0, 0));
	UVs.Add(FVector2D(1, 1));
	UVs.Add(FVector2D(1, 0));
}