#include "MiniBIM/BIMWall.h"
#include "MiniBIM/BIMGripPoint.h"
#include "MiniBIM/BIMProceduralMeshComponent.h"


const FVector ABIMWall::DEFAULT_WALL_DIMENSIONS = FVector(300, 50, 300);


void ABIMWall::GenerateMesh()
{
	Super::GenerateMesh();
}


float ABIMWall::GetGripPointsBaseHeight() const
{
	return 0.0f;
}

/*
* Spawn grip points at the extremities of the wall 
* Wall dimensions: (X, Y, Z)
* GripPoints locations: (+-X/2, 0, 0)
*/
void ABIMWall::SpawnGripPoints()
{
	Super::SpawnGripPoints();

	FVector dim(GetDimensions().X * 0.5f, 0, GetGripPointsBaseHeight());

	SpawnGripPoint(TEXT("GripPoint_0"), this, dim, FRotator::ZeroRotator);
	SpawnGripPoint(TEXT("GripPoint_1"), this, FVector(-dim.X, 0, dim.Z), FRotator::ZeroRotator);
}


void ABIMWall::DestroyGripPoints()
{
	Super::DestroyGripPoints();
}


/*
* Update wall dimensions, location and rotation based on grip points position.
* Called from InputStateGrab on Update method.
*/
void ABIMWall::UpdateFromGripPoints(UBIMGripPoint* movingGripPoint)
{
	Super::UpdateFromGripPoints(movingGripPoint);

	UBIMGripPoint* grabbedGripPoint = movingGripPoint;
	UBIMGripPoint* fixedGripPoint = nullptr;
	bool invertRotation = false;

	if (_GripPoints.Find(movingGripPoint))
	{
		invertRotation = true;
		fixedGripPoint = _GripPoints[0];
	}
	else
	{
		invertRotation = false;
		fixedGripPoint = _GripPoints[1];
	}
	
	if (!grabbedGripPoint || !fixedGripPoint)
		return;

	FVector fixedPointLocation = fixedGripPoint->GetComponentLocation();
	fixedPointLocation.Z = 0;
	FVector grabbedPointLocation = grabbedGripPoint->GetComponentLocation();
	grabbedPointLocation.Z = 0;

	float edge = FVector::Dist(grabbedPointLocation, fixedPointLocation);

	FVector wallLocation = (fixedPointLocation + grabbedPointLocation) * 0.5f;
	wallLocation.Z = 0;

	FRotator wallRotation = invertRotation ? (fixedPointLocation - grabbedPointLocation).Rotation() : (grabbedPointLocation - fixedPointLocation).Rotation();

	FVector newDimensions = FVector(edge, GetDimensions().Y, GetDimensions().Z);

	SetDimensions(newDimensions);
	SetActorRotation(wallRotation);
	SetActorLocation(wallLocation);
	RefreshGripPoints();
}


/*
* Set correct position of already existing grip points.
*/
void ABIMWall::RefreshGripPoints()
{
	FVector dim(GetDimensions().X * 0.5f, 0, GetGripPointsBaseHeight());

	_GripPoints[0]->SetRelativeLocationAndRotation(FVector(dim.X, 0, dim.Z), FRotator::ZeroRotator);
	_GripPoints[1]->SetRelativeLocationAndRotation(FVector(-dim.X, 0, dim.Z), FRotator::ZeroRotator);
}