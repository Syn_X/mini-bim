#include "MiniBIM/BIMFloatingPawn.h"
#include <MiniBIM/ProceduralBaseActor.h>
#include <MiniBIM/BIMPlayerController.h>
#include <MiniBIM/BIMGameMode.h>
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "GameFramework/PlayerInput.h"
#include "GameFramework/FloatingPawnMovement.h"


// Sets default values
ABIMFloatingPawn::ABIMFloatingPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bAddDefaultMovementBindings = false;

	UFloatingPawnMovement* movementComponent = Cast<UFloatingPawnMovement>(GetMovementComponent());
	if (movementComponent)
		movementComponent->MaxSpeed = 3000.0f;

	_PhysicsHandle = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("PhysicsHandle"));
}


// Called when the game starts or when spawned
void ABIMFloatingPawn::BeginPlay()
{
	Super::BeginPlay();
	SetActorEnableCollision(false);
	_InputState = &_StateSelect;
}


// Called every frame
void ABIMFloatingPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	_InputState->Update(DeltaTime);
}


// Called to bind functionality to input
void ABIMFloatingPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InitializeAxisMapping();

	// ADefaultPawn clone bindings
	PlayerInputComponent->BindAxis("Pawn_MoveForward", this, &ADefaultPawn::MoveForward);
	PlayerInputComponent->BindAxis("Pawn_MoveRight", this, &ADefaultPawn::MoveRight);
	PlayerInputComponent->BindAxis("Pawn_MoveUp", this, &ADefaultPawn::MoveUp_World);

	// ADefaultPawn clone bindings with intermediate check to prevent camera rotation when left mouse button is clicked
	PlayerInputComponent->BindAxis("Pawn_Turn", this, &ABIMFloatingPawn::Turn);
	PlayerInputComponent->BindAxis("Pawn_TurnRate", this, &ABIMFloatingPawn::TurnRate);
	PlayerInputComponent->BindAxis("Pawn_LookUp", this, &ABIMFloatingPawn::LookUp);
	PlayerInputComponent->BindAxis("Pawn_LookUpRate", this, &ABIMFloatingPawn::LookUpRate);

	// Custom pawn bindings
	PlayerInputComponent->BindAction("LeftMouseButton", IE_Pressed, this, &ABIMFloatingPawn::OnLeftMouseClick);
	PlayerInputComponent->BindAction("LeftMouseButton", IE_Released, this, &ABIMFloatingPawn::OnLeftMouseRelease);

	PlayerInputComponent->BindAction("Delete", IE_Pressed, this, &ABIMFloatingPawn::DeleteSelectedObjects);

	PlayerInputComponent->BindAction("SelectPlacementMode", IE_Pressed, this, &ABIMFloatingPawn::SetPlacementModeSelect);
	PlayerInputComponent->BindAction("WallPlacementMode", IE_Pressed, this, &ABIMFloatingPawn::SetPlacementModeWall);
	PlayerInputComponent->BindAction("SlabPlacementMode", IE_Pressed, this, &ABIMFloatingPawn::SetPlacementModeSlab);
	PlayerInputComponent->BindAction("PillarPlacementMode", IE_Pressed, this, &ABIMFloatingPawn::SetPlacementModePillar);
	PlayerInputComponent->BindAction("GrabPlacementMode", IE_Pressed, this, &ABIMFloatingPawn::SetPlacementModeGrab);
	PlayerInputComponent->BindAction("MEPPlacementMode", IE_Pressed, this, &ABIMFloatingPawn::SetPlacementModeMEP);

	PlayerInputComponent->BindAction("RightMouseButton", IE_Pressed, this, &ABIMFloatingPawn::OnRightMouseClick);

}


void ABIMFloatingPawn::InitializeAxisMapping()
{
	static bool bBindingsAdded = false;
	if (!bBindingsAdded)
	{
		bBindingsAdded = true;
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveForward", EKeys::W, 1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveForward", EKeys::S, -1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveForward", EKeys::Up, 1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveForward", EKeys::Down, -1.f));

		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveRight", EKeys::A, -1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveRight", EKeys::D, 1.f));

		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveUp", EKeys::LeftControl, -1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveUp", EKeys::SpaceBar, 1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveUp", EKeys::E, 1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_MoveUp", EKeys::Q, -1.f));

		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_TurnRate", EKeys::Left, -1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_TurnRate", EKeys::Right, 1.f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_Turn", EKeys::MouseX, 1.f));

		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Pawn_LookUp", EKeys::MouseY, -1.f));

		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("RightMouseButton", EKeys::RightMouseButton));
	}
}


/*
*	Passes left mouse click event to the current state
*/
void ABIMFloatingPawn::OnLeftMouseClick()
{
	FHitResult hitResult;
	if (!GetHitFromMousePointer(hitResult))
		return;

	_InputState->HandleLeftMouseClick(hitResult);
}


/*
*	Passes right mouse click event to the current state
*/
void ABIMFloatingPawn::OnRightMouseClick()
{
	FHitResult hitResult;
	if (!GetHitFromMousePointer(hitResult))
		return;

	_InputState->HandleRightMouseClick(hitResult);
}


/*
*	Passes left mouse release event to the current state
*/
void ABIMFloatingPawn::OnLeftMouseRelease()
{
	_InputState->HandleLeftMouseRelease();
}


UPhysicsHandleComponent* ABIMFloatingPawn::GetPhysicsHandle() const
{
	return _PhysicsHandle;
}


void ABIMFloatingPawn::DeleteSelectedObjects()
{
	if (_InputState == &_StateSelect)
	{
		_StateSelect.DeleteSelectedObjects();
	}
}


const TArray<AProceduralBaseActor*> ABIMFloatingPawn::GetSelectedObjects()
{
	if (_InputState == &_StateSelect)
	{
		return _StateSelect.GetSelectedObjects();
	}

	return TArray<AProceduralBaseActor*>();
}


void ABIMFloatingPawn::SetPlacementModeWall()
{
	SetPlacementMode(&_StateWall);
}


void ABIMFloatingPawn::SetPlacementModeSlab()
{
	SetPlacementMode(&_StateSlab);
}


void ABIMFloatingPawn::SetPlacementModePillar()
{
	SetPlacementMode(&_StatePillar);
}


void ABIMFloatingPawn::SetPlacementModeGrab()
{
	SetPlacementMode(&_StateGrab);
}


void ABIMFloatingPawn::SetPlacementModeSelect()
{
	SetPlacementMode(&_StateSelect);
}


void ABIMFloatingPawn::SetPlacementModeMEP()
{
	SetPlacementMode(&_StateMEP);
}


void ABIMFloatingPawn::SetPlacementMode(InputState* state)
{
	if (state == _InputState)
		return;

	_InputState->Exit();

	_InputState = state;
}


/*
*	Prevent call to parent method if middle mouse button is not down
*/
void ABIMFloatingPawn::Turn(const float axis)
{
	const APlayerController* PC = Cast<APlayerController>(GetController());
	if (!PC)
		return;

	if (PC->IsInputKeyDown(EKeys::MiddleMouseButton))
		Super::AddControllerYawInput(axis);
}


/*
*	Prevent call to parent method if middle mouse button is not down
*/
void ABIMFloatingPawn::TurnRate(const float axis)
{
	const APlayerController* PC = Cast<APlayerController>(GetController());
	if (!PC)
		return;

	if (PC->IsInputKeyDown(EKeys::MiddleMouseButton))
		Super::TurnAtRate(axis);
}


/*
*	Prevent call to parent method if middle mouse button is not down
*/
void ABIMFloatingPawn::LookUp(const float axis)
{
	const APlayerController* PC = Cast<APlayerController>(GetController());
	if (!PC)
		return;

	if (PC->IsInputKeyDown(EKeys::MiddleMouseButton))
		Super::AddControllerPitchInput(axis);
}


/*
*	Prevent call to parent method if middle mouse button is not down
*/
void ABIMFloatingPawn::LookUpRate(const float axis)
{
	const APlayerController* PC = Cast<APlayerController>(GetController());
	if (!PC)
		return;

	if (PC->IsInputKeyDown(EKeys::MiddleMouseButton))
		Super::LookUpAtRate(axis);
}


void ABIMFloatingPawn::CancelAction()
{
	_InputState->Exit();
}


bool ABIMFloatingPawn::GetHitFromMousePointer(FHitResult& hitResult)
{
	const APlayerController* PC = Cast<APlayerController>(GetController());
	if (!PC)
		return false;

	return PC->GetHitResultUnderCursor(ECollisionChannel::ECC_WorldDynamic, false, hitResult);
}