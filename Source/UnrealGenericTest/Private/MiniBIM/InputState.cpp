#include "MiniBIM/InputState.h"
#include "CoreMinimal.h"
#include "MiniBIM/ProceduralBaseActor.h"
#include "MiniBIM/BIMWall.h"
#include "MiniBIM/BIMSlab.h"
#include "MiniBIM/BIMPillar.h"
#include "MiniBIM/BIMFloatingPawn.h"
#include "MiniBIM/BIMGripPoint.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "UnrealGenericTest.h"

#pragma region InputState

InputState::InputState(const ABIMFloatingPawn& pawn)
{
	_Pawn = &pawn;

	_InputModeGameAndUI = FInputModeGameAndUI();
	_InputModeGameAndUI.SetHideCursorDuringCapture(false);
	_InputModeGameAndUI.SetWidgetToFocus(nullptr);
	_InputModeGameAndUI.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	
	_InputModeGameOnly = FInputModeGameOnly();
	_InputModeGameOnly.SetConsumeCaptureMouseDown(false);

}


void InputState::Exit()
{
	ResetClickLocations();
	_ClickState = INPUT_WAIT_FIRST_CLICK;
	if (_SpawnedObject)
	{
		_SpawnedObject->Destroy();
		_SpawnedObject = nullptr;
	}
}


void InputState::HandleRightMouseClick(const FHitResult& hitResult)
{
	Exit();
}

/*
*	Internal click state machine updater. Used for grabbing and wall/slab placement.
*	Both INPUT_WAIT_FIRST_CLICK and INPUT_WAIT_SECOND_CLICK states call Execute method, that handles click management to the subclass.
*/
void InputState::UpdateClickStateMachine(const FHitResult& hitResult)
{
	switch (_ClickState)
	{
		case EClickState::INPUT_WAIT_FIRST_CLICK:
		{
			_FirstClickLocation = FVector(hitResult.Location.X, hitResult.Location.Y, 0);
			_SecondClickLocation = FVector(hitResult.Location.X, hitResult.Location.Y, 0);
			_ClickState = EClickState::INPUT_DRAGGING;
			break;
		}
		case EClickState::INPUT_WAIT_SECOND_CLICK:
		{
			_SecondClickLocation = FVector(hitResult.Location.X, hitResult.Location.Y, 0);
			_ClickState = EClickState::INPUT_WAIT_FIRST_CLICK;

			Execute();
			ResetClickLocations();
			_SpawnedObject = nullptr;
			break;
		}
		default:
		{
			_ClickState = EClickState::INPUT_WAIT_FIRST_CLICK;
			ResetClickLocations();
			_SpawnedObject = nullptr;
			return;
			break;
		}
	}

}


void InputState::ResetClickLocations()
{
	_FirstClickLocation = FVector::ZeroVector;
	_SecondClickLocation = FVector::ZeroVector;
}


/*
*	Helper function to prevent code replication.
*	Returns true if mouse cursor hits something dynamic, with the FHitResult as an output parameter.
*/
bool InputState::GetHitFromMousePointer(FHitResult& hitResult, const TArray<AActor*>& ignoredActors)
{

	const APlayerController* PC = Cast<APlayerController>(_Pawn->GetController());
	if (!PC)
		return false;

	for (auto it : ignoredActors)
	{
		it->SetActorEnableCollision(false);
	}

	bool retVal = PC->GetHitResultUnderCursor(ECollisionChannel::ECC_WorldStatic, false, hitResult);

	for (auto it : ignoredActors)
	{
		it->SetActorEnableCollision(true);
	}

	return retVal;
}

#pragma endregion


#pragma region InputStateSelect

InputStateSelect::InputStateSelect(const ABIMFloatingPawn& pawn) : InputState(pawn)
{
	_StateGripPoint = new InputStateGripPoint(pawn);
}


/*
*	InputStateSelect updates _GrabbedComponent position on Update, called on BIMFloatingPawn Tick.
*	If _GrabbedComponent is a BIMGripPoint, calls UpdateFromGripPoints method to let the actor update itstransform.
*/
void InputStateSelect::Update(float DeltaSeconds)
{
	if (_StateGripPoint->IsGrabbing())
		_StateGripPoint->Update(DeltaSeconds);
}


/*
*	InputStateSelect handles:
*		- left mouse click to select/deselect USelectableInterface items
*		- left mouse drag to drag BIMGripPoints
*	Dragging is performed with a UPhysicsHandleComponent on the BIMGripPoint.
*/
void InputStateSelect::HandleLeftMouseClick(const FHitResult& hitResult)
{
	if (!hitResult.GetActor()->IsRootComponentMovable())
	{
		DeselectAllObjects();
		return;
	}

	// Grab grip points only
	if (hitResult.GetComponent()->GetAttachParent()->IsA(UBIMGripPoint::StaticClass()))
	{
		_StateGripPoint->HandleLeftMouseClick(hitResult);
		return;
	}

	// Selection
	if (hitResult.GetActor()->GetClass()->ImplementsInterface(USelectableInterface::StaticClass()))
	{
		AProceduralBaseActor* selectedObj = Cast<AProceduralBaseActor>(hitResult.GetActor());

		if (!selectedObj)
			return;

		if (GetSelectedObjects().Contains(selectedObj))
		{
			RemoveObjectFromSelectedArray(selectedObj);
			selectedObj->OnDeselected();
		}
		else
		{
			AddObjectToSelectedArray(Cast<AProceduralBaseActor>(hitResult.GetActor()));
			selectedObj->OnSelected();
		}
		return;
	}
}


/*
*	Release _GrabbedComponent on left mouse release.
*/
void InputStateSelect::HandleLeftMouseRelease()
{
	if(_StateGripPoint->IsGrabbing())
		_StateGripPoint->HandleLeftMouseRelease();
}


void InputStateSelect::Exit()
{
	InputState::Exit();
	_StateGripPoint->Exit();
	DeselectAllObjects();
}


void InputStateSelect::DeleteSelectedObjects()
{
	for (auto it : _SelectedObjects)
	{
		it->DestroyGripPoints();
		_Pawn->GetWorld()->DestroyActor(it);
	}
	_SelectedObjects.Empty();
}

const TArray<AProceduralBaseActor*> InputStateSelect::GetSelectedObjects()
{
	return _SelectedObjects;
}

void InputStateSelect::DeselectAllObjects()
{
	for (auto it : _SelectedObjects) {
		it->OnDeselected();
	}

	_SelectedObjects.Empty();
}


void InputStateSelect::AddObjectToSelectedArray(AProceduralBaseActor* object)
{
	_SelectedObjects.Add(object);
}


void InputStateSelect::RemoveObjectFromSelectedArray(AProceduralBaseActor* object)
{
	_SelectedObjects.Remove(object);
}



#pragma endregion


#pragma region InputStateGrab

bool InputStateGrab::IsGrabbing()
{
	return _GrabbedComponent != nullptr;
}


/*
*	InputStateGrab updates _GrabbedComponent position on Update, called on BIMFloatingPawn Tick.
*	If _GrabbedComponent is a BIMGripPoint, calls UpdateFromGripPoints method to let the actor update itstransform.
*/
void InputStateGrab::Update(float DeltaSeconds)
{
	if (!IsGrabbing())
		return;

	FHitResult hitResult;
	if (!GetHitFromMousePointer(hitResult))
		return;

	FVector hitLocation = hitResult.Location;

	FVector newLocation = FTransform(_RelativeGrabLocation).InverseTransformPosition(hitLocation);
	USceneComponent* root = _GrabbedComponent->GetAttachmentRoot();
	newLocation.Z = root->GetComponentLocation().Z;
	_GrabbedComponent->GetAttachmentRoot()->SetWorldLocation(newLocation);
}


/*
*	InputStateGrab handles left mouse click to start dragging.
*	Dragging is performed with a UPhysicsHandleComponent on the BIMFloatingPawn.
*/
void InputStateGrab::HandleLeftMouseClick(const FHitResult& hitResult)
{
	// Not movable --> not grabbable --> not selectable --> exit
	if (!hitResult.GetActor()->IsRootComponentMovable())
		return;

	if (!hitResult.GetActor()->IsA(AProceduralBaseActor::StaticClass()))
		return;

	_GrabbedComponent = hitResult.GetComponent();

	FVector hitLocation = hitResult.Location;

	_Pawn->GetPhysicsHandle()->GrabComponentAtLocation(_GrabbedComponent, NAME_None, hitLocation);
	_RelativeGrabLocation = FTransform(_GrabbedComponent->GetComponentLocation()).InverseTransformPosition(hitLocation);
}


/*
*	Release _GrabbedComponent on left mouse release.
*/
void InputStateGrab::HandleLeftMouseRelease()
{
	ReleaseComponent();
}


void InputStateGrab::Exit()
{
	InputState::Exit();
	ReleaseComponent();
}


void InputStateGrab::ReleaseComponent()
{
	if (IsGrabbing())
	{
		_Pawn->GetPhysicsHandle()->ReleaseComponent();
		_GrabbedComponent->GetAttachmentRootActor()->SetActorEnableCollision(true);
		_GrabbedComponent = nullptr;
	}
}

#pragma endregion


#pragma region InputStateGripPoint
/*
*	InputStateGripPoint is an internal state of InputStateSelect to manage GripPoints dragging.
*	It handles grabbing in the same way as InputStateGrab, but specializes calls to manage GripPoints and their structure.
*/


bool InputStateGripPoint::IsGrabbing()
{
	return _GrabbedComponent != nullptr;
}


/*
*	InputStateGrab updates _GrabbedComponent position on Update, called on BIMFloatingPawn Tick.
*	If _GrabbedComponent is a BIMGripPoint, calls UpdateFromGripPoints method to let the actor update itstransform.
*/
void InputStateGripPoint::Update(float DeltaSeconds)
{
	if (!IsGrabbing())
		return;

	AProceduralBaseActor* parentActor = Cast<AProceduralBaseActor>(_GrabbedComponent->GetAttachmentRootActor());
	if (!parentActor)
		return;

	TArray<AActor*> toIgnore = TArray<AActor*>();
	toIgnore.Add(parentActor);

	FHitResult hitResult;
	if (!GetHitFromMousePointer(hitResult, toIgnore))
		return;

	FVector hitLocation = hitResult.Location;
	hitLocation.Z = parentActor->GetGripPointsBaseHeight();
	FVector newLocation = FTransform(_RelativeGrabLocation).InverseTransformPosition(hitLocation);

	newLocation.Z = parentActor->GetGripPointsBaseHeight();
	_GrabbedComponent->SetWorldLocation(newLocation);
	parentActor->UpdateFromGripPoints(_GrabbedComponent);	
}


/*
*	InputStateGripPoint handles left mouse click to start dragging.
*	Dragging is performed with a UPhysicsHandleComponent on the BIMFloatingPawn.
*/
void InputStateGripPoint::HandleLeftMouseClick(const FHitResult& hitResult)
{
	if (!hitResult.GetComponent()->GetAttachParent()->IsA(UBIMGripPoint::StaticClass()))
		return;

	_GrabbedComponent = Cast<UBIMGripPoint>(hitResult.GetComponent()->GetAttachParent());

	FVector hitLocation = hitResult.Location;
	hitLocation.Z = 0;
	_Pawn->GetPhysicsHandle()->GrabComponentAtLocation(Cast<UPrimitiveComponent>(_GrabbedComponent), NAME_None, hitLocation);
	_RelativeGrabLocation = FTransform(_GrabbedComponent->GetComponentLocation()).InverseTransformPosition(hitLocation);
}


/*
*	Release _GrabbedComponent on left mouse release.
*/
void InputStateGripPoint::HandleLeftMouseRelease()
{
	if (_GrabbedComponent)
	{
		_Pawn->GetPhysicsHandle()->ReleaseComponent();
		_GrabbedComponent = nullptr;
	}
}


void InputStateGripPoint::Exit()
{
	if (_GrabbedComponent)
	{
		_GrabbedComponent = nullptr;
		_Pawn->GetPhysicsHandle()->ReleaseComponent();
	}

}
#pragma endregion


#pragma region InputStateWall

/*
*	Uses ClickStateMachine of InputState base class.
*/
void InputStateWall::HandleLeftMouseClick(const FHitResult& hitResult)
{
	UpdateClickStateMachine(hitResult);
}


/*
*	Spawn wall if release point is valid: it should be in INPUT_DRAGGING state, release must happen inside building floor and wall dimension must be over a threshold.
*/
void InputStateWall::HandleLeftMouseRelease()
{
	// I'm not dragging, exit
	if (_ClickState != EClickState::INPUT_DRAGGING)
		return;

	TArray<AActor*> toIgnore = TArray<AActor*>();
	if (_SpawnedObject)
		toIgnore.Add(_SpawnedObject);

	// Release dragging
	FHitResult hitResult;
	if (!GetHitFromMousePointer(hitResult, toIgnore))
	{
		_SecondClickLocation = FVector::ZeroVector;
		_ClickState = EClickState::INPUT_WAIT_SECOND_CLICK;
		return;
	}

	FVector hitLocation = FVector(hitResult.Location.X, hitResult.Location.Y, 0);

	// Handle release of a single click
	if (FVector::Dist(_FirstClickLocation, hitLocation) < _DragMinThreshold)
	{
		_SecondClickLocation = FVector::ZeroVector;
		_ClickState = EClickState::INPUT_WAIT_SECOND_CLICK;
		return;
	}

	// Spawn
	_SecondClickLocation = hitLocation;
	Execute();

	_SpawnedObject = nullptr;
	_ClickState = EClickState::INPUT_WAIT_FIRST_CLICK;

}


void InputStateWall::Execute()
{
	SpawnWall();
}


/*
*	Called to show wall preview during construction.
*/
void InputStateWall::Update(float DeltaTime)
{
	if (_ClickState == EClickState::INPUT_WAIT_FIRST_CLICK)
		return;

	TArray<AActor*> toIgnore = TArray<AActor*>();
	if (_SpawnedObject)
		toIgnore.Add(_SpawnedObject);

	FHitResult hitResult;
	if (!GetHitFromMousePointer(hitResult, toIgnore))
		return;

	const FVector hitLocation = FVector(hitResult.Location.X, hitResult.Location.Y, 0);
	_SecondClickLocation = hitLocation;
	Execute();
}


void InputStateWall::SpawnWall()
{
	UWorld* world = _Pawn->GetWorld();

	float edge = FVector::Dist(_SecondClickLocation, _FirstClickLocation);

	FVector wallLocation = (_SecondClickLocation + _FirstClickLocation) * 0.5;
	wallLocation.Z = 0;

	FRotator wallRotation = (_SecondClickLocation - _FirstClickLocation).Rotation();


	if (_SpawnedObject != nullptr) 
	{
		_SpawnedObject->SetActorLocation(wallLocation);
		_SpawnedObject->SetActorRotation(wallRotation);
		_SpawnedObject->SetDimensions(FVector(edge, ABIMWall::DEFAULT_WALL_DIMENSIONS.Y, ABIMWall::DEFAULT_WALL_DIMENSIONS.Z));
	}
	else
	{
		const FTransform spawnTransform = FTransform(wallRotation, wallLocation);
		_SpawnedObject = world->SpawnActorDeferred<ABIMWall>(ABIMWall::StaticClass(), spawnTransform);
		_SpawnedObject->SetDimensions(FVector(edge, ABIMWall::DEFAULT_WALL_DIMENSIONS.Y, ABIMWall::DEFAULT_WALL_DIMENSIONS.Z));
		_SpawnedObject->FinishSpawning(spawnTransform);
	}

}

#pragma endregion


#pragma region InputStateSlab


void InputStateSlab::HandleLeftMouseClick(const FHitResult& hitResult)
{
	UpdateClickStateMachine(hitResult);
}


void InputStateSlab::HandleLeftMouseRelease()
{
	// I'm not dragging, exit
	if (_ClickState != EClickState::INPUT_DRAGGING)
		return;

	// Handle release of a single click
	if (FVector::Dist(_FirstClickLocation, _SecondClickLocation) < _DragMinThreshold)
	{
		_SecondClickLocation = FVector::ZeroVector;
		_ClickState = EClickState::INPUT_WAIT_SECOND_CLICK;
		return;
	}


	TArray<AActor*> toIgnore = TArray<AActor*>();
	if (_SpawnedObject)
		toIgnore.Add(_SpawnedObject);

	// Release dragging --> spawn
	FHitResult hitResult;
	if (!GetHitFromMousePointer(hitResult, toIgnore))
	{
		_SecondClickLocation = FVector::ZeroVector;
		_ClickState = EClickState::INPUT_WAIT_SECOND_CLICK;
		return;
	}

	_SecondClickLocation = FVector(hitResult.Location.X, hitResult.Location.Y, 0);
	SpawnSlab();
	_SpawnedObject = nullptr;
	_ClickState = EClickState::INPUT_WAIT_FIRST_CLICK;

}

void InputStateSlab::Update(float DeltaTime)
{
	if (_ClickState == EClickState::INPUT_WAIT_FIRST_CLICK)
		return;

	TArray<AActor*> toIgnore = TArray<AActor*>();
	if (_SpawnedObject)
		toIgnore.Add(_SpawnedObject);

	FHitResult hitResult;
	if (!GetHitFromMousePointer(hitResult, toIgnore))
		return;

	_SecondClickLocation = FVector(hitResult.Location.X, hitResult.Location.Y, 0);
	Execute();
}

void InputStateSlab::Execute()
{
	SpawnSlab();
}


void InputStateSlab::SpawnSlab()
{
	UWorld* world = _Pawn->GetWorld();
	if (!world)
		return;

	float xEdge = (_SecondClickLocation.X - _FirstClickLocation.X);
	float yEdge = (_SecondClickLocation.Y - _FirstClickLocation.Y);

	FVector slabLocation = (_SecondClickLocation + _FirstClickLocation) * 0.5;
	slabLocation.Z = 0;

	FRotator slabRotation = FRotator::ZeroRotator;

	if (_SpawnedObject != nullptr) {
		_SpawnedObject->SetActorLocation(slabLocation);
		_SpawnedObject->SetActorRotation(slabRotation);
		_SpawnedObject->SetDimensions(FVector(xEdge, yEdge, ABIMSlab::DEFAULT_SLAB_DIMENSIONS.Z));
	}
	else
	{
		const FTransform spawnTransform = FTransform(slabRotation, slabLocation);
		_SpawnedObject = world->SpawnActorDeferred<ABIMSlab>(ABIMSlab::StaticClass(), spawnTransform);
		_SpawnedObject->SetDimensions(FVector(xEdge, yEdge, ABIMSlab::DEFAULT_SLAB_DIMENSIONS.Z));
		_SpawnedObject->FinishSpawning(spawnTransform);
	}
}

#pragma endregion


#pragma region InputStatePillar

/*
*	Spawn Pillar directly on click.
*/
void InputStatePillar::HandleLeftMouseClick(const FHitResult& hitResult)
{
	UWorld* world = _Pawn->GetWorld();
	if (!world)
		return;

	FVector pillarLocation = FVector(hitResult.Location.X, hitResult.Location.Y, 0);

	FRotator pillarRotation = FRotator::ZeroRotator;

	const FTransform spawnTransform = FTransform(pillarRotation, pillarLocation);
	ABIMPillar* spawnedActor = world->SpawnActorDeferred<ABIMPillar>(ABIMPillar::StaticClass(), spawnTransform);
	spawnedActor->SetDimensions(ABIMPillar::DEFAULT_PILLAR_DIMENSIONS);
	spawnedActor->FinishSpawning(spawnTransform);
}

#pragma endregion



#pragma region InputStateMEP

InputStateMEP::InputStateMEP(const ABIMFloatingPawn& pawn) : InputState(pawn)
{
}


/*
*	InputStateMEP Update checks if other entities are added between _StartingObject and _EndingObject , called on BIMFloatingPawn Tick.
*	If _GrabbedComponent is a BIMGripPoint, calls UpdateFromGripPoints method to let the actor update itstransform.
*/
void InputStateMEP::Update(float DeltaSeconds)
{
	if (!_StartingObject || !_EndingObject)
		return;
	
	CheckPath();
}


/*
*	InputStateMEP handles left mouse click to select/deselect USelectableInterface items as the starting and ending point of the path to find.
*/
void InputStateMEP::HandleLeftMouseClick(const FHitResult& hitResult)
{
	if (!hitResult.GetActor()->IsRootComponentMovable())
	{
		DeselectAllObjects();
		return;
	}

	// Selection
	if (hitResult.GetActor()->GetClass()->ImplementsInterface(USelectableInterface::StaticClass()))
	{
		AProceduralBaseActor* selectedObj = Cast<AProceduralBaseActor>(hitResult.GetActor());
		
		if (selectedObj == _StartingObject)
		{
			_StartingObject->OnDeselected();
			_StartingObject = nullptr;
			if(_EndingObject)
				Swap(_StartingObject, _EndingObject);
			return;
		}

		if (selectedObj == _EndingObject)
		{
			_EndingObject->OnDeselected();
			_EndingObject = nullptr;
			return;
		}

		if (!_StartingObject)
		{
			_StartingObject = selectedObj;
			_StartingObject->OnSelected();
			return;
		}
		
		if (!_EndingObject)
		{
			_EndingObject = selectedObj;
			_EndingObject->OnSelected();
			CheckPath();
			return;
		}
		return;
	}
}


void InputStateMEP::CheckPath()
{
	_OpenList.Empty();
	_ClosedList.Empty();

	if (IsDestination(_StartingObject) == true)
		return;

	ActorNode startingNode = ActorNode();
	startingNode.Actor = _StartingObject;
	startingNode.hCost = FVector::Dist(_StartingObject->GetActorLocation(), _EndingObject->GetActorLocation());
	startingNode.gCost = 0;
	startingNode.fCost = startingNode.gCost + startingNode.hCost;

	ActorNode tempNode = ActorNode();
	for (auto it : _StartingObject->GetNeighbours())
	{
		tempNode.Actor = it;
		tempNode.hCost = FVector::Dist(it->GetActorLocation(), _EndingObject->GetActorLocation());
		tempNode.gCost = FVector::Dist(it->GetActorLocation(), _StartingObject->GetActorLocation());
		tempNode.fCost = tempNode.gCost + tempNode.hCost;
		_OpenList.AddUnique(tempNode);
	}

	bool destinationFound = false;

	_ClosedList.Add(startingNode); // startingNode is the first node of the path

	while (_OpenList.Num() != 0 && !destinationFound) 
	{
		ActorNode* nextNode = nullptr;
		for (auto n : _OpenList)
		{
			if (IsDestination(n.Actor))
			{
				destinationFound = true;
				nextNode = &n; // one of the neighbours is the destination, mission completed
				break;
			}

			// find neighbour with lowest fCost
			float temp = FLT_MAX;
			if (n.fCost < temp) {
				temp = n.fCost;
				nextNode = &n;
			}
		}

		if (!nextNode)
		{
			UE_LOG(LogMiniBIM, Log, TEXT("No nextNode found, error!"));
			break;
		}

		_ClosedList.AddUnique(*nextNode);
		_OpenList.Empty();
		
		ActorNode tmp = ActorNode();
		for (auto it : nextNode->Actor->GetNeighbours())
		{
			bool shouldContinue = false;
			for (auto cIt : _ClosedList) 
			{
				if (cIt.Actor == it)
					shouldContinue = true;
			}
			if (shouldContinue)
				continue;

			tmp.Actor = it;
			tmp.hCost = FVector::Dist(it->GetActorLocation(), _EndingObject->GetActorLocation());
			tmp.gCost = FVector::Dist(it->GetActorLocation(), _StartingObject->GetActorLocation());
			tmp.fCost = tmp.gCost + tmp.hCost;
			_OpenList.AddUnique(tmp);
		}
	}

	if (destinationFound == false) {
		UE_LOG(LogMiniBIM, Log, TEXT("Path not found"));
		return;
	}
	else
	{
		for (auto it : _ClosedList)
		{
			it.Actor->OnSelected();
		}
	}

}


void InputStateMEP::Exit()
{
	InputState::Exit();

	DeselectAllObjects();
}


void InputStateMEP::DeselectAllObjects()
{
	if (_StartingObject)
	{
		_StartingObject->OnDeselected();
		_StartingObject = nullptr;
	}
	if (_EndingObject) 
	{
		_EndingObject->OnDeselected();
		_EndingObject = nullptr;
	}
	if (_ClosedList.Num() > 0)
	{
		for (auto it : _ClosedList)
		{
			it.Actor->OnDeselected();
		}
	}
}


/*
*	Returns whether the destination actor is the same as current actor 
*/
bool InputStateMEP::IsDestination(AProceduralBaseActor* currentActor)
{
	if (currentActor == _EndingObject)
		return true;

	return false;
}

#pragma endregion

