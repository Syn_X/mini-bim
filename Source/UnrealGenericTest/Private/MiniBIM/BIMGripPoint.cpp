#include "MiniBIM/BIMGripPoint.h"

// Sets default values
UBIMGripPoint::UBIMGripPoint()
{
	PrimaryComponentTick.bCanEverTick = false;


	_Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SphereMesh"));
	_Mesh->SetupAttachment(GetAttachmentRoot());

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	_Mesh->SetStaticMesh(SphereMeshAsset.Object);

	ConstructorHelpers::FObjectFinder<UMaterial> Material(TEXT("Material'/Game/MiniBIM/Materials/M_GripPoints.M_GripPoints'"));
	if (Material.Object != NULL)
	{
		_Material = (UMaterial*)Material.Object;
	}
	_Material_Dyn = UMaterialInstanceDynamic::Create(_Material, _Mesh);
	_Mesh->SetMaterial(0, _Material_Dyn);

	_Mesh->SetWorldScale3D(FVector(1.25f));
	_Mesh->SetRenderCustomDepth(true);
	_Mesh->CustomDepthStencilValue = 3;

}


void UBIMGripPoint::Init()
{
	_Mesh->RegisterComponent();
}


void UBIMGripPoint::OnUnregister()
{
	Super::OnUnregister();
	_Mesh->UnregisterComponent();
	_Mesh->DestroyComponent();
}


void UBIMGripPoint::ResetLocation()
{
	_Mesh->SetRelativeLocation(FVector(0, 0, _Mesh->GetRelativeLocation().Z));
	this->SetRelativeLocation(FVector(0, 0, this->GetRelativeLocation().Z));
}


float UBIMGripPoint::GetGripPointRadius()
{
	return _Mesh->GetStaticMesh()->GetBounds().GetSphere().W;
}
