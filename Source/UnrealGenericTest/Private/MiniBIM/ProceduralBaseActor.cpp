#include "MiniBIM/ProceduralBaseActor.h"
#include "MiniBIM/BIMGripPoint.h"
#include "BIMProceduralMeshComponent.h"
#include "Components/BoxComponent.h"

// Sets default values
AProceduralBaseActor::AProceduralBaseActor()
{
	PrimaryActorTick.bCanEverTick = false;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Scene;

	_Mesh = CreateDefaultSubobject<UBIMProceduralMeshComponent>(TEXT("BIMProceduralMesh"));
	_Mesh->SetupAttachment(RootComponent);

	_Box = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionDetectionBoundingBox"));
	_Box->SetupAttachment(RootComponent);
	_Box->OnComponentBeginOverlap.AddDynamic(this, &AProceduralBaseActor::AddNeighbour);
	_Box->OnComponentEndOverlap.AddDynamic(this, &AProceduralBaseActor::RemoveNeighbour);
	_Box->SetVisibility(true);
	_Box->SetHiddenInGame(false);
	UpdateBoundingBox();
}


void AProceduralBaseActor::PostActorCreated()
{
	Super::PostActorCreated();
	GenerateMesh();

}


void AProceduralBaseActor::PostLoad()
{
	Super::PostLoad();
	GenerateMesh();
}


void AProceduralBaseActor::OnSelected()
{
	_Selected = true;
	_Mesh->Select();
	SpawnGripPoints();
}


void AProceduralBaseActor::OnDeselected()
{
	_Selected = false;
	_Mesh->Deselect();
	DestroyGripPoints();
}


void AProceduralBaseActor::SpawnGripPoint(const FName& name, UObject* outer, const FVector& location, const FRotator& rotation)
{
	_GripPoints.Add(NewObject<UBIMGripPoint>(outer, name));
	UBIMGripPoint* lastAdded = _GripPoints[_GripPoints.Num() - 1];
	lastAdded->RegisterComponent();
	lastAdded->SetRelativeLocationAndRotation(location, rotation);
	lastAdded->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	lastAdded->Init();
}


void AProceduralBaseActor::DestroyGripPoints()
{
	for (auto it : _GripPoints)
	{
		it->UnregisterComponent();
		it->DestroyComponent();
	}
	_GripPoints.Empty();
}


float AProceduralBaseActor::GetGripPointsBaseHeight() const
{
	return 0.0f;
}


TArray<AProceduralBaseActor*> AProceduralBaseActor::GetNeighbours() const
{
	return _Neighbours;
}


void AProceduralBaseActor::AddNeighbour(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherActor->IsA(AProceduralBaseActor::StaticClass()))
	{
		AProceduralBaseActor* actor = Cast<AProceduralBaseActor>(OtherActor);
		_Neighbours.AddUnique(actor);
	}
}


void AProceduralBaseActor::RemoveNeighbour(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherActor->IsA(AProceduralBaseActor::StaticClass()))
	{
		AProceduralBaseActor* actor = Cast<AProceduralBaseActor>(OtherActor);
		_Neighbours.Remove(actor);
	}
}


void AProceduralBaseActor::UpdateGripPoints()
{
	DestroyGripPoints();
	SpawnGripPoints();
}


void AProceduralBaseActor::UpdateBoundingBox()
{
	_Box->SetBoxExtent(GetDimensionsForBoundingBox());
	FVector newLocation = FVector(0, 0, GetDimensions().Z * 0.5f);
	_Box->SetRelativeLocation(newLocation);
}


FVector AProceduralBaseActor::GetDimensionsForBoundingBox() const
{
	return GetDimensions() * 0.5;
}


void AProceduralBaseActor::GenerateMesh()
{
	_Mesh->ResetMesh();
	_Mesh->GenerateMesh();
	UpdateBoundingBox();
}


void AProceduralBaseActor::SetDimensions(const FVector& dimensions)
{
	_Mesh->SetDimensions(dimensions);
	GenerateMesh();

	if (_Selected)
		UpdateGripPoints();
}


void AProceduralBaseActor::SetDimensionsFromText(const FText& x, const FText& y, const FText& z)
{
	static const FString comma = ",";
	static const FString empty = "";
	
	auto stringX = x.ToString().Replace(*comma, *empty);
	auto stringY = y.ToString().Replace(*comma, *empty);
	auto stringZ = z.ToString().Replace(*comma, *empty);

	if (stringX.IsNumeric() && stringY.IsNumeric() && stringZ.IsNumeric())
	{
		auto floatX = FCString::Atof(*stringX);
		auto floatY = FCString::Atof(*stringY);
		auto floatZ = FCString::Atof(*stringZ);

		const FVector newDimensions = FVector(floatX, floatY, floatZ);
		SetDimensions(newDimensions);
	}
}


/*
*	Returns FVector with the dimensions of the procedural mesh.
*/
FVector AProceduralBaseActor::GetDimensions() const
{
	return _Mesh->GetDimensions();
}


/*
*	Helper function callable from Blueprint, needed to interact with UI widgets.
*	Update position from FText variables.
*/
void AProceduralBaseActor::SetPositionFromText(const FText& x, const FText& y, const FText& z)
{
	static const FString comma = ",";
	static const FString empty = "";

	FString stringX = x.ToString().Replace(*comma, *empty);
	FString stringY = y.ToString().Replace(*comma, *empty);
	FString stringZ = z.ToString().Replace(*comma, *empty);

	if (stringX.IsNumeric() && stringY.IsNumeric() && stringZ.IsNumeric())
	{
		float floatX = FCString::Atof(*stringX);
		float floatY = FCString::Atof(*stringY);
		float floatZ = FCString::Atof(*stringZ);

		const FVector newPosition = FVector(floatX, floatY, floatZ);
		SetActorLocation(newPosition, false, nullptr, ETeleportType::ResetPhysics);
	}
}
