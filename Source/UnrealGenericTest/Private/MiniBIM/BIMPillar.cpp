#include "MiniBIM/BIMPillar.h"
#include "MiniBIM/BIMGripPoint.h"


const FVector ABIMPillar::DEFAULT_PILLAR_DIMENSIONS = FVector(50, 50, 300);


float ABIMPillar::GetGripPointsBaseHeight() const
{
	return 0;
}


void ABIMPillar::GenerateMesh()
{
	Super::GenerateMesh();
}


/*
* Spawn grip points at the edges of the slab
* Pillar dimensions: (X, Y, Z)
* GripPoint location: (0, 0, 0)
*/
void ABIMPillar::SpawnGripPoints()
{
	Super::SpawnGripPoints();

	SpawnGripPoint(TEXT("GripPoint"), this, FVector::ZeroVector, FRotator::ZeroRotator);
}


/*/*
* Update pillar location based on grip point position.
* Called from InputStateGrab on Update method.
*/
void ABIMPillar::UpdateFromGripPoints(UBIMGripPoint* movingGripPoint)
{
	Super::UpdateFromGripPoints(movingGripPoint);
	FVector newLocation = GetActorLocation() + _GripPoints[0]->GetRelativeLocation();
	newLocation.Z = GetActorLocation().Z;
	_GripPoints[0]->ResetLocation();
	SetActorLocation(newLocation);
}
