#include "MiniBIM/BIMSlab.h"
#include "MiniBIM/BIMGripPoint.h"
#include "Math/UnrealMathUtility.h"


const FVector ABIMSlab::DEFAULT_SLAB_DIMENSIONS = FVector(100, 100, 50);


void ABIMSlab::GenerateMesh()
{
	Super::GenerateMesh();
}


float ABIMSlab::GetGripPointsBaseHeight() const
{
	return 0.0f;
}


/*
* Spawn grip points at the edges of the slab
* Slab dimensions: (X, Y, Z)
* GripPoints locations: (+-X/2, 0, 0) and (0, +-Y/2, 0)
*/
void ABIMSlab::SpawnGripPoints()
{
	Super::SpawnGripPoints();

	const FVector dim = FVector(GetDimensions().X * 0.5, GetDimensions().Y * 0.5, GetGripPointsBaseHeight());

	SpawnGripPoint(TEXT("GripPoint_E"), this, FVector(dim.X, 0, dim.Z), FRotator::ZeroRotator);
	SpawnGripPoint(TEXT("GripPoint_W"), this, FVector(-dim.X, 0, dim.Z), FRotator::ZeroRotator);
	SpawnGripPoint(TEXT("GripPoint_S"), this, FVector(0, dim.Y, dim.Z), FRotator::ZeroRotator);
	SpawnGripPoint(TEXT("GripPoint_N"), this, FVector(0, -dim.Y, dim.Z), FRotator::ZeroRotator);
}


void ABIMSlab::DestroyGripPoints()
{
	Super::DestroyGripPoints();

}


/*
* Update slab dimensions, location and rotation based on grip points position.
* Called from InputStateGrab on Update method.
*/
void ABIMSlab::UpdateFromGripPoints(UBIMGripPoint* movingGripPoint)
{
	Super::UpdateFromGripPoints(movingGripPoint);

	if (_GripPoints[0]->GetRelativeLocation().X <= _GripPoints[1]->GetRelativeLocation().X + 200.0f)
	{
		RefreshGripPoints();
		return;
	}

	if (_GripPoints[2]->GetRelativeLocation().Y <= _GripPoints[3]->GetRelativeLocation().Y + 200.0f)
	{
		RefreshGripPoints();
		return;
	}

	const FVector actorLocation = GetActorLocation();

	const float dimX =  FMath::Abs(_GripPoints[0]->GetRelativeLocation().X) + FMath::Abs(_GripPoints[1]->GetRelativeLocation().X);
	const float dimY = FMath::Abs(_GripPoints[3]->GetRelativeLocation().Y) + FMath::Abs(_GripPoints[2]->GetRelativeLocation().Y);

	const float cenX = actorLocation.X + (_GripPoints[0]->GetRelativeLocation().X + _GripPoints[1]->GetRelativeLocation().X) * 0.5;
	const float cenY = actorLocation.Y + (_GripPoints[3]->GetRelativeLocation().Y + _GripPoints[2]->GetRelativeLocation().Y) * 0.5;
	const FVector newLocation = FVector(cenX, cenY, 0);

	SetDimensions(FVector(dimX, dimY, GetDimensions().Z));
	SetActorLocation(newLocation);

	RefreshGripPoints();
}


/*
* Set correct position of already existing grip points.
*/
void ABIMSlab::RefreshGripPoints()
{
	const FVector dim = FVector(GetDimensions().X * 0.5, GetDimensions().Y * 0.5, GetGripPointsBaseHeight());

	_GripPoints[0]->SetRelativeLocationAndRotation(FVector(dim.X, 0, dim.Z), FRotator::ZeroRotator);
	_GripPoints[1]->SetRelativeLocationAndRotation(FVector(-dim.X, 0, dim.Z), FRotator::ZeroRotator);
	_GripPoints[2]->SetRelativeLocationAndRotation(FVector(0, dim.Y, dim.Z), FRotator::ZeroRotator);
	_GripPoints[3]->SetRelativeLocationAndRotation(FVector(0, -dim.Y, dim.Z), FRotator::ZeroRotator);
}
