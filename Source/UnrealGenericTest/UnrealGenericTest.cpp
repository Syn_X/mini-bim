// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealGenericTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealGenericTest, "UnrealGenericTest" );
DEFINE_LOG_CATEGORY(LogMiniBIM);
