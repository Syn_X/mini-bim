#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "MiniBIM/ProceduralBaseActor.h"

#include "BIMSlab.generated.h"

UCLASS()
class UNREALGENERICTEST_API ABIMSlab : public AProceduralBaseActor
{
	GENERATED_BODY()

public:

	static const FVector DEFAULT_SLAB_DIMENSIONS;

	float GetGripPointsBaseHeight() const override;

protected:
	void GenerateMesh() override;

	void SpawnGripPoints() override;

	void DestroyGripPoints() override;

	void UpdateFromGripPoints(UBIMGripPoint* movingGripPoint) override;

	void RefreshGripPoints() override;

};