#pragma once
#include <cfloat>

struct FHitResult;
struct FVector;
class UPhysicsHandleComponent;
class UPrimitiveComponent;
class ABIMFloatingPawn;
class AProceduralBaseActor;
class UBIMGripPoint;

/*
*	State machine classes to manage different input modes.
*	States are executed from BIMFloatingPawn class.
* 
*	InputState: base class with helper and shared methods.
*	InputStateSelect: manages items selection, enabling user to edit parameters and delete items.
*	InputStateGrab: lets user drag items around.
*	InputStateGripPoint: enables grip points, lets user drag grip points to edit items shape.
*	InputStateWall/Slab/Pillar: lets user place a wall, a slab or a pillar with mouse click or drag.
*/


class InputState
{
public:
	InputState(const ABIMFloatingPawn& pawn);

	virtual ~InputState() {}

	virtual void Update(float DeltaTime) {};							// Called every tick

	virtual void Exit();												// Called on state exit

	virtual void HandleLeftMouseClick(const FHitResult& hitResult) {};	// Called on left mouse click

	virtual void HandleLeftMouseRelease() {};							// Called on left mouse release

	virtual void HandleRightMouseClick(const FHitResult& hitResult);	// Called on right mouse click

	virtual void HandleRightMouseRelease() {};							// Called on right mouse release

protected:
	virtual void UpdateClickStateMachine(const FHitResult& hitResult);

	virtual void Execute() {};

	void ResetClickLocations();
	
	bool GetHitFromMousePointer(FHitResult& hitResult, const TArray<AActor*>& ignoredActors = TArray<AActor*>());

	enum EClickState
	{
		INPUT_WAIT_FIRST_CLICK,
		INPUT_WAIT_SECOND_CLICK,
		INPUT_DRAGGING
	};
	EClickState _ClickState = INPUT_WAIT_FIRST_CLICK;

	FVector _FirstClickLocation = FVector::ZeroVector;
	FVector _SecondClickLocation = FVector::ZeroVector;

	const ABIMFloatingPawn* _Pawn;

	AProceduralBaseActor* _SpawnedObject = nullptr;

	FInputModeGameAndUI _InputModeGameAndUI;
	FInputModeGameOnly _InputModeGameOnly;

};


class InputStateGrab : public InputState
{
public:

	InputStateGrab(const ABIMFloatingPawn& pawn) : InputState(pawn) {};

	void Update(float DeltaTime) override;

	void HandleLeftMouseClick(const FHitResult& hitResult) override;

	void HandleLeftMouseRelease() override;

	void Exit() override;

	bool IsGrabbing();

private:

	void ReleaseComponent();

	UPhysicsHandleComponent* _PhysicsHandle = nullptr;
	UPrimitiveComponent* _GrabbedComponent = nullptr;

	FVector _RelativeGrabLocation;
};



class InputStateGripPoint : public InputState
{
public:

	InputStateGripPoint(const ABIMFloatingPawn& pawn) : InputState(pawn) {};

	void Update(float DeltaTime) override;

	void HandleLeftMouseClick(const FHitResult& hitResult) override;

	void HandleLeftMouseRelease() override;

	void Exit() override;

	bool IsGrabbing();

private:

	UBIMGripPoint* _GrabbedComponent = nullptr;

	FVector _RelativeGrabLocation;
};


class InputStateSelect : public InputState
{
public:
	InputStateSelect(const ABIMFloatingPawn& pawn);

	void Update(float DeltaTime) override;

	void HandleLeftMouseClick(const FHitResult& hitResult) override;

	void HandleLeftMouseRelease() override;

	void Exit() override;

	void DeleteSelectedObjects();

	const TArray<AProceduralBaseActor*> GetSelectedObjects();
private:

	void DeselectAllObjects();

	TArray<AProceduralBaseActor*> _SelectedObjects;


	void AddObjectToSelectedArray(AProceduralBaseActor* object);

	void RemoveObjectFromSelectedArray(AProceduralBaseActor* object);

	InputStateGripPoint* _StateGripPoint = nullptr;
};


class InputStateWall : public InputState
{
public:
	InputStateWall(const ABIMFloatingPawn& pawn) : InputState(pawn) {};

	void HandleLeftMouseClick(const FHitResult& hitResult) override;

	void HandleLeftMouseRelease() override;

	void Update(float DeltaTime) override;

protected:
	void Execute() override;

	void SpawnWall();

	float _DragMinThreshold = 200.0f;
};


class InputStateSlab : public InputState
{
public:
	InputStateSlab(const ABIMFloatingPawn& pawn) : InputState(pawn) {};

	void HandleLeftMouseClick(const FHitResult& hitResult) override;

	void HandleLeftMouseRelease() override;

	void Update(float DeltaTime) override;

protected:
	void Execute() override;

	void SpawnSlab();

	float _DragMinThreshold = 200.0f;
};


class InputStatePillar : public InputState
{
public:
	InputStatePillar(const ABIMFloatingPawn& pawn) : InputState(pawn) {};

	void HandleLeftMouseClick(const FHitResult& hitResult) override;
};


class ActorNode
{
public:
	AProceduralBaseActor* Actor = nullptr;
	float gCost = FLT_MAX;
	float hCost = FLT_MAX;
	float fCost = FLT_MAX;

	bool operator==(const ActorNode& a) const
	{
		return this->Actor == a.Actor;
	}
};


class InputStateMEP : public InputState
{
public:
	InputStateMEP(const ABIMFloatingPawn& pawn);

	void Update(float DeltaTime) override;

	void HandleLeftMouseClick(const FHitResult& hitResult) override;

	void Exit() override;


private:

	void DeselectAllObjects();

	void CheckPath();

	bool IsDestination(AProceduralBaseActor* currentActor);

	float CalculateHValue(AProceduralBaseActor* currentActor);

	AProceduralBaseActor* _StartingObject = nullptr;
	
	AProceduralBaseActor* _EndingObject = nullptr;

	TArray<ActorNode> _OpenList;
	TArray<ActorNode> _ClosedList;
};