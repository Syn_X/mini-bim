#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "MiniBIM/ProceduralBaseActor.h"

#include "BIMPillar.generated.h"

UCLASS()
class UNREALGENERICTEST_API ABIMPillar : public AProceduralBaseActor
{
	GENERATED_BODY()

public:

	static const FVector DEFAULT_PILLAR_DIMENSIONS;

	float GetGripPointsBaseHeight() const override;

protected:
	void GenerateMesh() override;

	void SpawnGripPoints() override;

	void UpdateFromGripPoints(UBIMGripPoint* movingGripPoint) override;

};