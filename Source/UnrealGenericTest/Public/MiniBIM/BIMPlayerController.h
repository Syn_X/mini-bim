// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "BIMPlayerController.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class UNREALGENERICTEST_API ABIMPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ABIMPlayerController();

protected:
	void BeginPlay() override;

};
