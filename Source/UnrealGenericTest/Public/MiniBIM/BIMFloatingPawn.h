#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "MiniBIM/InputState.h"

#include "BIMFloatingPawn.generated.h"

class ABIMGameMode;
class AProceduralBaseActor;
class UPhysicsHandleComponent;
class UPrimitiveComponent;

/*
*	Pawn that manages:
*	- inputs with the InputState FSM
*	- selected objects
*	- objects and grip points dragging 
*/

UCLASS()
class ABIMFloatingPawn : public ADefaultPawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABIMFloatingPawn();

protected:
	// Called when the game starts or when spawned
	void BeginPlay() override;

public:	
	// Called every frame
	void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Grabber
	UPhysicsHandleComponent* GetPhysicsHandle() const;

	UFUNCTION(BlueprintCallable)
	void DeleteSelectedObjects();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<AProceduralBaseActor*> GetSelectedObjects();


private:
	void OnLeftMouseClick();

	void OnRightMouseClick();

	void OnLeftMouseRelease();

	UFUNCTION(BlueprintCallable, Category = "BIM Placement Mode")
	void SetPlacementModeWall();
	UFUNCTION(BlueprintCallable, Category = "BIM Placement Mode")
	void SetPlacementModeSlab();
	UFUNCTION(BlueprintCallable, Category = "BIM Placement Mode")
	void SetPlacementModePillar();
	UFUNCTION(BlueprintCallable, Category = "BIM Placement Mode")
	void SetPlacementModeGrab();
	UFUNCTION(BlueprintCallable, Category = "BIM Placement Mode")
	void SetPlacementModeSelect();
	UFUNCTION(BlueprintCallable, Category = "BIM Placement Mode")
	void SetPlacementModeMEP();

	void SetPlacementMode(InputState* state);

	void InitializeAxisMapping();

	void Turn(float axis);
	void TurnRate(float axis);
	void LookUp(float axis);
	void LookUpRate(float axis);

	void CancelAction();
	bool GetHitFromMousePointer(FHitResult& hitResult);

	InputState* _InputState;
	InputStateSelect _StateSelect = InputStateSelect(*this);
	InputStateGrab _StateGrab = InputStateGrab(*this);
	InputStateMEP _StateMEP = InputStateMEP(*this);
	InputStateWall _StateWall = InputStateWall(*this);
	InputStateSlab _StateSlab = InputStateSlab(*this);
	InputStatePillar _StatePillar = InputStatePillar(*this);

	UPhysicsHandleComponent* _PhysicsHandle;
};
