// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SelectableInterface.generated.h"

/**
 * Interface for selectable objects
 */
UINTERFACE(MinimalAPI, Blueprintable)
class USelectableInterface : public UInterface
{
	GENERATED_BODY()
};


class ISelectableInterface
{
	GENERATED_BODY()

public:

	virtual void OnSelected() = 0;
	virtual void OnDeselected() = 0;
};