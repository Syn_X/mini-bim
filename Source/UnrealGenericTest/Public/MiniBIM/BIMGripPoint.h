#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "BIMGripPoint.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UNREALGENERICTEST_API UBIMGripPoint : public USceneComponent
{
	GENERATED_BODY()
	
public:	
	UBIMGripPoint();

	void Init();

	void OnUnregister() override;

	void ResetLocation();

	float GetGripPointRadius();

private:
	UPROPERTY(VisibleAnywhere, Category = "Grip Points Component")
	UStaticMeshComponent* _Mesh;

	UMaterial* _Material;

	UMaterialInstanceDynamic* _Material_Dyn;

	UMaterial* _Material_Selected;

	UMaterialInstanceDynamic* _Material_Dyn_Selected;
};
