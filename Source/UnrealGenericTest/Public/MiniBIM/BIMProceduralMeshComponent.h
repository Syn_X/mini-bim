#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "ProceduralMeshComponent.h"
#include "BIMProceduralMeshComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UNREALGENERICTEST_API UBIMProceduralMeshComponent : public UPrimitiveComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBIMProceduralMeshComponent();

	void ResetMesh();

	void Select();

	void Deselect();

	void GenerateMesh();

	void SetDimensions(FVector dimensions);

	const FVector GetDimensions();

private:
	void AddTriangleMesh(FVector TopRight, FVector BottomRight, FVector BottomLeft, int32& TriIndex, FProcMeshTangent Tangent);
	void AddQuadMesh(FVector TopRight, FVector BottomRight, FVector TopLeft, FVector BottomLeft, int32& TriIndex, FProcMeshTangent Tangent);

	UMaterial* _Material;

	UProceduralMeshComponent* _Mesh;

	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FProcMeshTangent> Tangents;
	TArray<FVector2D> UVs;
	TArray<FLinearColor> Colors;

	FVector _Dimensions = FVector(0, 0, 0);
};
