#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "MiniBIM/ProceduralBaseActor.h"

#include "BIMWall.generated.h"

UCLASS()
class UNREALGENERICTEST_API ABIMWall : public AProceduralBaseActor
{
	GENERATED_BODY()

public:

	static const FVector DEFAULT_WALL_DIMENSIONS;

	float GetGripPointsBaseHeight() const override;

protected:
	void GenerateMesh() override;

	void SpawnGripPoints() override;

	void DestroyGripPoints() override;

	void UpdateFromGripPoints(UBIMGripPoint* movingGripPoint) override;

	void RefreshGripPoints() override;

};