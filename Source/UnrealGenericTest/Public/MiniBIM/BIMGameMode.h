#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "BIMGameMode.generated.h"

// Forward declaration
class ABIMPlayerController;
class AProceduralBaseActor;


/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class UNREALGENERICTEST_API ABIMGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
 
public:
	ABIMGameMode();

protected:
	void BeginPlay( ) override;

};
