#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MiniBIM/SelectableInterface.h"

#include "ProceduralBaseActor.generated.h"

class UBIMProceduralMeshComponent;
class UBIMGripPoint;
class UBoxComponent;

/*
*	Base Actor that contains a BIMProceduralMeshComponent and an array of BIMGripPoint.
*	Every entity inherits from this class, and has to override only GenerateMesh(), SpawnGripPoints() and UpdateFromGripPoints() methods.
*/

UCLASS()
class UNREALGENERICTEST_API AProceduralBaseActor : public AActor, public ISelectableInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProceduralBaseActor();

	// ISelectableInterface functions
	void OnSelected() override;
	void OnDeselected() override;

	UFUNCTION(BlueprintCallable, Category = "Edit Parameters")
	virtual void SetDimensions(const FVector& dimensions);
	UFUNCTION(BlueprintCallable, Category = "Edit Parameters")
	virtual void SetDimensionsFromText(const FText& x, const FText& y, const FText& z);

	UFUNCTION(BlueprintCallable, Category = "Edit Parameters")
	virtual void UpdateFromGripPoints(UBIMGripPoint* movingGripPoint) {};

	UFUNCTION(BlueprintPure, Category = "Edit Parameters")
	FVector GetDimensions() const;

	UFUNCTION(BlueprintCallable, Category = "Edit Parameters")
	virtual void SetPositionFromText(const FText& x, const FText& y, const FText& z);

	virtual void SpawnGripPoints() {};
	virtual void SpawnGripPoint(const FName& name, UObject* outer, const FVector& location, const FRotator& rotation);
	virtual void DestroyGripPoints();
	virtual void RefreshGripPoints() {};

	virtual float GetGripPointsBaseHeight() const;

	TArray<AProceduralBaseActor*> GetNeighbours() const;
	UFUNCTION()
	void AddNeighbour(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void RemoveNeighbour(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:

	void PostActorCreated() override;
	void PostLoad() override;

	virtual void GenerateMesh();

	void UpdateGripPoints();

	void UpdateBoundingBox();

	FVector GetDimensionsForBoundingBox() const;

	USceneComponent* Scene;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mesh Component")
	UBIMProceduralMeshComponent* _Mesh;	

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Collision Detection Component")
	UBoxComponent* _Box;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grip Points Component")
	TArray<UBIMGripPoint*> _GripPoints;

	TArray<AProceduralBaseActor*> _Neighbours;

	bool _Selected = false;

};
