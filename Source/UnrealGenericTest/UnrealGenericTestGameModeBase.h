// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealGenericTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREALGENERICTEST_API AUnrealGenericTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
